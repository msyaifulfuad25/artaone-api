<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMuser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muser', function (Blueprint $table) {
            $table->integer('noid')->length(11);
            $table->string('kode', 20);
            $table->string('nama', 100);
            $table->string('user_email', 100);
            $table->string('user_mobile', 20);
            $table->string('nik', 20);
            $table->string('alamat1', 100);
            $table->string('alamat2', 100);
            $table->string('kota', 100);
            $table->string('propinsi', 100);
            $table->string('negara', 100);
            $table->string('ao_username', 20);
            $table->string('ao_password', 60);
            $table->string('dice_username', 60);
            $table->string('dice_password', 60);
            $table->timestamp('docreate')->useCurrent();
            $table->timestamp('doactivated')->useCurrent();
            $table->timestamp('dolastlogin')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('muser');
    }
}
