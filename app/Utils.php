<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use DB;

class Utils extends Model {

    static public function nextNoid($table) {
        $result = DB::table($table)->orderBy('noid','desc')->first();
        return $result ? $result->noid+1 : 1;
    }

}
