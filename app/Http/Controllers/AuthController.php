<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\MUser;
use App\Utils;
use App\Http\Controllers\DiceAuthController;

class AuthController extends Controller {

    static private $key = 'ac434795726f4c04856dec509c703f80';

    public function index(\Illuminate\Http\Request $request) {
        return 'index';
        $dice = DiceAuthController::createAccount();
        
        if ($dice) {
            $request->session()->put('SessionCookie', $dice['SessionCookie']);
            return $request->session()->get('SessionCookie');
        }
        return false;
    }

    public function register(Request $request) {
        $nama = @$request->input('nama');
        $user_email = @$request->input('user_email');
        $user_mobile = @$request->input('user_mobile');
        $nik = @$request->input('nik');
        $alamat1 = @$request->input('alamat1');
        $alamat2 = @$request->input('alamat2');
        $kota = @$request->input('kota');
        $propinsi = @$request->input('propinsi');
        $negara = @$request->input('negara');
        $username = @$request->input('username');
        $password = @$request->input('password');

        if (!$nama) return response()->json(['success' => false, 'message' => 'Nama is required']);
        if (!$user_email) return response()->json(['success' => false, 'message' => 'User Email is required']);
        if (!$user_mobile) return response()->json(['success' => false, 'message' => 'User Mobile is required']);
        if (!$nik) return response()->json(['success' => false, 'message' => 'NIK is required']);
        if (!$alamat1) return response()->json(['success' => false, 'message' => 'Alamat 1 is required']);
        if (!$alamat2) return response()->json(['success' => false, 'message' => 'Alamat 2 is required']);
        if (!$kota) return response()->json(['success' => false, 'message' => 'Kota is required']);
        if (!$propinsi) return response()->json(['success' => false, 'message' => 'Propinsi is required']);
        if (!$negara) return response()->json(['success' => false, 'message' => 'Negara is required']);
        if (!$username) return response()->json(['success' => false, 'message' => 'Username is required']);
        if (!$password) return response()->json(['success' => false, 'message' => 'Password is required']);
        $generateUsername = md5($username.date('YmdHis'));
        $generatePassword = md5($password.date('YmdHis'));

        $createAccount = DiceAuthController::createAccount();

        if (!$createAccount) return response()->json(['success' => false, 'message' => $createAccount]);
        
        $createUser = DiceAuthController::createUser([
            'a' => 'CreateUser',
            's' => $createAccount['SessionCookie'],
            'Username' => $generateUsername,
            'Password' => $generatePassword
        ]);

        $form = [
            'noid' => Utils::nextNoid('muser'),
            'kode' => Str::random(6),
            'nama' => $nama ? $nama : '',
            'user_email' => $user_email ? $user_email : '',
            'user_mobile' => $user_mobile ? $user_mobile : '',
            'nik' => $nik ? $nik : '',
            'alamat1' => $alamat1 ? $alamat1 : '',
            'alamat2' => $alamat2 ? $alamat2 : '',
            'kota' => $kota ? $kota : '',
            'propinsi' => $propinsi ? $propinsi : '',
            'negara' => $negara ? $negara : '',
            'ao_username' => $username,
            'ao_password' => Hash::make($password),
            'dice_username' => $generateUsername,
            'dice_password' => $generatePassword,
        ];

        $insert = MUser::insert($form);

        if (!$insert)
            return response()->json([
                'success' => false,
                'message' => 'Insert Error'
            ]);

        return response()->json([
            'success' => true,
            'message' => 'Successful',
        ]);
    }

    public function login(Request $request) {
        $username = @$request->input('username');
        $password = @$request->input('password');

        if (!$username) return response()->json(['success' => false, 'message' => 'Username is required']);
        if (!$password) return response()->json(['success' => false, 'message' => 'Password is required']);

        $user = MUser::where('ao_username',$username)->first();

        if (!$user) return response()->json(['success' => false, 'message' => 'Username not found']);
        if (!Hash::check($password, $user->ao_password)) return response()->json(['success' => false, 'message' => 'Wrong Password']);
        
        $dice = DiceAuthController::login([
            'a' => 'Login',
            'Key' => self::$key,
            'Username' => $user->dice_username,
            'Password' => $user->dice_password,
        ]);

        if (@$dice['error']) {
            return response()->json(['success' => false, 'message' => $dice['error']]);
        } elseif (@$dice['InvalidApiKey']) {
            return response()->json(['success' => false, 'message' => 'Invalid Api Key']);
        } elseif (@$dice['LoginInvalid']) {
            return response()->json(['success' => false, 'message' => 'Login Invalid']);
        }

        return response()->json([
            'success' => true,
            'message' => 'Successful',
            'data' => $dice
        ]);
    }

}
