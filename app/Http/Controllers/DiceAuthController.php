<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use App\MUser;

class DiceAuthController extends Controller {

    static private $url = 'https://www.999doge.com/api/web.aspx';
    static private $key = 'ac434795726f4c04856dec509c703f80';

    static public function createAccount() {
        $body = [
            'a' => 'CreateAccount',
            'Key' => self::$key
        ];
        $post = HTTP::asForm()->post(self::$url, $body);

        switch($post) {
            case $post->status() == 200:
                $response = $post->json();
                break;
        }
        return $response;
    }

    static public function createUser($params) {
        $post = HTTP::asForm()->post(self::$url, $params);

        if (@$post->json()['AccountHasUser']) return response()->json(['success' => true, 'message' => 'Account Has User']);
        if (@$post->json()['UsernameTaken']) return response()->json(['success' => true, 'message' => 'Username Taken']);
     
        return response()->json([
            'success' => true,
            'message' => 'Successful'
        ]);
    }

    static public function login($params) {
        $post = HTTP::asForm()->post(self::$url, $params);
        return $post->json();
    }

}
